# Interview-java-20221216-PhanNguyenDangTruong


Đề bài: Xây dựng server ứng dụng restful viết API cho các chức năng sau
- Quản lý người dùng (danh sách, thêm, xóa, sửa, tìm kiếm theo email)
- Chức năng đăng nhập / đăng xuất
- Chức năng quên mật khẩu cho người dùng (gửi qua email)
- Chức năng kích hoạt tài khoản sau khi người dùng được thêm vào hệ thống (gửi qua email)
- Khởi tạo sẵn dữ liệu cho từng loại người dùng

Yêu cầu bài toán:
- Tất cả các tính năng quản lý người dùng yêu cầu phải đăng nhập
- Có 2 loại người dùng là admin / client
- Người dùng sẽ nhập lại mật khẩu sau khi kích hoạt tài khoản từ email
- Mật khẩu lưu trong db phải được hash hoặc mã hóa
- Chỉ người dùng là admin thì mới được thực hiện việc (thêm, xóa, sửa)
- Chức năng danh sách và tìm kiếm phải có phân trang

